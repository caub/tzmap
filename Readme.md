## Write an application that shows time in different timezones

- User must be able to create an account and log in. (If a mobile application, this means that more users can use the app from the same phone).
- When logged in, a user can see, edit and delete timezones he entered.
- Implement at least three roles with different permission levels: a regular user would only be able to CRUD on their owned records, a user manager would be able to CRUD users, and an admin would be able to CRUD all records and users.
- When a timezone is entered, each entry has a Name, Name of the city in timezone, the difference to GMT time.
- When displayed, each entry also has current time.
- Filter by names.
- REST API. Make it possible to perform all user actions via the API, including authentication (If a mobile application and you don’t know how to create your own backend you can use Firebase.com or similar services to create the API).
- In any case, you should be able to explain how a REST API works and demonstrate that by creating functional tests that use the REST Layer directly. Please be prepared to use REST clients like Postman, cURL, etc. for this purpose.
- All actions need to be done client side using AJAX, refreshing the page is not acceptable. (If a mobile app, disregard this).
- Minimal UI/UX design is needed. You will not be marked on graphic design. However, do try to keep it as tidy as possible.
- Bonus: unit and e2e tests.


### Installation

- requires to run a local postgresql instance (9.6+), with postgis extension (you might need a `sudo apt install postgis` if on linux, or similar on mac, windows I don't know)
- `npm i`: install deps, you need node 8+ (LTS)
- `npm run build`: build app for production (run `ORIGIN=http://localhost:3000 npm run build` then `node server` to test it locally)
- `npm start` to start app in development mode, browse http://localhost:9000 (use `npx concurrently ".." ".."` if your OS doesn't allow &)
- `npm test` test app (`npx jest --watch` to run it in watch mode)

### Description

- server
It serves an API, with express, sessions. The DB is postgresql with this [schema](migrations/20171114130414_init.js).

  - Basic users can only see and manage their profile and timezones.

  - Admin have a few permissions keys (defined in `users.perm` column), all possible keys are defined in [FULL_PERM](server/config), the most powerful one is `write_user` combined with `write_perm`, it allows to modify user, including its permissions. Admins permissions are independent of users.

  - Managers control specific users with specific capacities (similar to admins, except `read_perm`, `write_perm`), it uses the `managers` table for the many-to-many users-managers relations

- app
  - I used React to make this SPA, there's no server-rendering, and the server API is separated from it

  - I used a leaflet map to display timezones


### Demo

url: https://tzmap.herokuapp.com


### Notes

- knex command is slightly modified, on postinstall, until https://github.com/tgriesser/knex/pull/2331 gets merged (needs tests)
- It uses OAuth2 (google/facebook) because it's easier (no passwords, we could also send tokens by emails, but it requires a mail api, or SSO)
- I'll use React, Redux, React-router, leaflet to show records on a map: like [this](http://joergdietrich.github.io/Leaflet.Terminator)
- I used a custom assert lib because Jest's expect isn't practical for me, node's assert.deepEqual isn't always reliable, power-assert could be an alternative (need transpiling)
- it's often better to save all API events, so less of a REST api, more of an audit log/versioning, and have views to reflect the latest values of users, zones, ..

### Todos

- Use eslint:recommended, react-app as eslint extensions, and clean .eslintrc
