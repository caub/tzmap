const { LOGIN_RE, EMAIL_RE } = require('../server/config');

/**
 * init tables:
 * - users: all user accounts
 * - managers: user <-> managers relationships
 * - zones: a user record, that can show the position, time and description..
 *
 * - sessions: for our express-sessions
 */
exports.up = async knex => {
  await knex.schema.createTable('users', table => {
    table.text('id').primary();
    table.text('email').notNullable().unique();
    table.text('login').notNullable().unique();
    table.jsonb('perm').defaultTo('{}');
    table.text('avatar');

    table.timestamp('created').defaultTo(knex.fn.now());
    table.timestamp('deleted');
  });

  await Promise.all([
    knex.schema.raw(`ALTER TABLE users ADD CONSTRAINT valid_email CHECK (email ~* '${EMAIL_RE.source.replace(/\?/g, '\\$&').replace(/'/g, `''`)}')`),
    knex.schema.raw(`ALTER TABLE users ADD CONSTRAINT valid_login CHECK (login ~* '${LOGIN_RE.source}')`, []),

    // case insensitive email and logins
    knex.schema.raw('CREATE UNIQUE INDEX IF NOT EXISTS login_idx ON users(lower(login))'),
    knex.schema.raw('CREATE UNIQUE INDEX IF NOT EXISTS email_idx ON users(lower(email))')
  ]);


  // users <-> managers many to many relations
  await knex.schema.createTable('managers', table => {
    table.text('user_id').references('users.id').onDelete('CASCADE');
    table.text('manager_id').references('users.id').onDelete('CASCADE');

    table.boolean('read_user').defaultTo(true);
    table.boolean('write_user').defaultTo(true);

    table.boolean('read_zone').defaultTo(true);
    table.boolean('write_zone').defaultTo(true);

    table.timestamp('created').defaultTo(knex.fn.now());
    table.timestamp('deleted');

    table.primary(['user_id', 'manager_id']);
  });
  // todo set contraints maybe: like CHECK(read_zone OR NOT write_zone) since write is stronger

  await knex.schema.raw(`ALTER TABLE managers ADD CONSTRAINT valid_manager CHECK (user_id != manager_id)`);


  await knex.schema.createTable('zones', table => {
    table.text('id').primary();
    table.text('user_id').references('users.id').onDelete('CASCADE');

    // some descriptive name
    table.text('name');

    // all those 3 fields are related to each other, when moving coords or setting a new address the others get updated
    table.text('address');
    table.specificType('coords', 'geography(POINT)');
    table.text('tz').defaultTo('Europe/London');
  });

  await knex.schema.createTable('sessions', table => {
    table.text('id').primary();
    table.text('user_id').references('users.id').onDelete('CASCADE').index();
    table.json('cookie');
    table.timestamp('expire').defaultTo(knex.raw(`NOW() + interval '1w'`)).index();
  });
};

exports.down = async knex => {
  await knex.schema.dropTable('zones');
  await knex.schema.dropTable('managers');

  await knex.schema.dropTable('sessions');
  await knex.schema.dropTable('users');
};
