const eq = require('deep-eq');
const startServer = require('..');
const { FULL_PERM } = require('../config');
const fetchMock = require('../util/mock-auth-fetch');
const { knex } = require('../util');
const { fetchHeaders: _fetchHeaders, toJson } = require('../util/fetchJson');

const PORT = 3001;
let server;

beforeAll(async () => {
  server = startServer(PORT, fetchMock);
  await knex('sessions').delete();
  await knex('managers').delete();
  await knex('zones').delete();
  await knex('users').delete();
});
afterAll(async () => {
  server.close();
  await knex.destroy();
});

const fetchHeaders = (url, opts) => _fetchHeaders(`http://localhost:${PORT}${url}`, opts);
const fetch = (url, opts) => fetchHeaders(url, opts).then(toJson);

it('should prevent access without auth', async () => {
  let r = await fetchHeaders('/user');
  eq(r.status, 403);
  r = await fetchHeaders('/user/456');
  eq(r.status, 403);
  r = await fetchHeaders('/zone');
  eq(r.status, 403);
});

it('should auth with google', async () => {
  // small hack, put auth info in accessToken
  const user = await fetch('/signin/google/' + JSON.stringify({ email: 'test@t.st', name: 'Test' }));
  eq(user.email, 'test@t.st');
  eq(user.login, 'Test');
  eq(user.perm, {});

  let r = await fetchHeaders('/user');
  eq(r.status, 200);
  const u = await toJson(r);
  eq(u.id, user.id);
  eq(u.email, user.email);
  eq(u.login, user.login);

  r = await fetchHeaders('/zone', { method: 'POST', body: { coords: [7.63, 43.001] } });
  eq(r.status, 200);
  let z = await toJson(r);
  eq(z.coords, [7.63, 43.001]);
  eq(z.user_id, user.id);
  eq(z.tz, 'Europe/Paris');

  r = await fetchHeaders('/zone/' + z.id, { method: 'PATCH', body: { coords: [2.12, 42.12], name: 'Spain' } });
  eq(r.ok, true);
  z = await fetch('/zone/' + z.id);
  eq(z.coords, [2.12, 42.12]);
  eq(z.name, 'Spain');
  eq(z.tz, 'Europe/Madrid');

  await fetchHeaders('/logout');
  r = await fetchHeaders('/user');
  eq(r.status, 403);
});

it('should give managers and admin more permissions', async () => {
  let adm = await fetch('/signin/google/' + JSON.stringify({ email: 'admin@t.st' }));
  // hack this guy as admin
  await knex('users').where('id', adm.id).update({ perm: JSON.stringify(FULL_PERM) });
  adm = await fetch('/user/' + adm.id);
  eq(adm.perm, FULL_PERM);

  const users = await fetch('/user', { method: 'SEARCH' });
  let user = users.find(o => o.email === 'test@t.st'); // grab the user created in previous test

  let man = await fetch('/signin/google/' + JSON.stringify({ email: 'man@t.st' }));
  // const whomai = await fetch('/user');
  let r = await fetchHeaders('/user/' + user.id);
  eq(r.ok, false);

  r = await fetchHeaders('/manager', { method: 'PUT', body: { manager_id: man.id, user_id: user.id, read_user: true, write_user: true, read_zone: true, write_zone: true } });
  eq(r.status, 403);
  // sign again as admin to make man, manager
  adm = await fetch('/signin/google/' + JSON.stringify({ email: 'admin@t.st' }));
  r = await fetchHeaders('/manager', { method: 'PUT', body: { manager_id: man.id, user_id: user.id, read_user: true, write_user: true, read_zone: true, write_zone: true } });
  eq(r.status, 200);
  const entry = await toJson(r);
  eq(entry.read_user, true);

  // sign as manager again
  man = await fetch('/signin/google/' + JSON.stringify({ email: 'man@t.st' }));
  r = await fetchHeaders('/user/' + user.id);
  eq(r.ok, true);

  r = await fetchHeaders('/user/' + user.id, { method: 'PATCH', body: { login: 'uuu' } });
  eq(r.ok, true);
  user = await fetch('/user/' + user.id);
  eq(user.login, 'uuu');
});
