const path = require('path');
const express = require('express');
const session = require('express-session');
const Store = require('./util/session-store');
// const { G_CLIENT_ID } = require('./config');

const app = express().disable('x-powered-by');
// app.use(require('./util/https-middleware')()); // filter out google and facebook maybe
app.use(require('compression')());

const root = path.resolve(path.join(__dirname, '..'));

app.use('/', express.static(root + '/dist')); // serve app production bundle
app.use('/', express.static(__dirname + '/public', { extensions: ['html'] }));

app.use(require('./util/cors-middleware'));

app.use(session({
  secret: 'some secret to set more seriously',
  key: '_tt',
  resave: true,
  saveUninitialized: false,
  cookie: {
    maxAge: 1 * 86400e3 // 1 day
  },
  store: Store(),
}));

// API routes
app.use('/user', require('./routes/user'));
app.use('/zone', require('./routes/zone'));
app.use('/manager', require('./routes/manager'));

// signout
app.get(['/signout', '/logout'], (req, res) => {
  req.session.destroy(err => {
    res.status(err ? 500 : 204).end();
  });
});

/**
 * start app
 * @param port (optional, defaults to process.env.PORT || 3000)
 * @param fetch (optional, defaults to node-fetch)
 * @returns server
 */
const start = (port = process.env.PORT || 3000, fetch = require('node-fetch')) => {
  // OAuth2 providers
  app.use('/signin', require('./routes/auth')(fetch));

  // fallback to app
  app.get('*', (req, res) => {
    res.sendFile(root + '/dist/index.html');
  });

  return app.listen(port);
};

module.exports = start;

if (!module.parent) { // if this module is directly invoked, start the server
  const server = start();
  console.log('listening', server.address().port, 'root', root);
}
