const shortid = require('shortid');
const { HttpError, gravatar } = require('../util');
const { knex } = require('../util');

/* cols:
- id
- email
- login
- perm // key => boolean jsonb mapping, possible keys: "read_user" (and list), "write_user" (and delete, we could add a separate key later), "read_perm": see user permissions, "write_perm" (most powerful permission, full-admin), "read_zone", "write_zone"
- avatar
- created
- deleted (not used yet)
*/


/**
 * get (or create) from oauth2 endpoint
 * @param {*} userObj: { email, avatar, name }
 */
exports.signin = async ({ email, avatar = gravatar(email), name = '' }) => {
  const [user] = await knex('users').whereRaw('LOWER(email)=LOWER(TRIM(?))', [email]);

  if (user) {
    return user;
  }

  const id = shortid();
  const _login = String(name || '').replace(/[^\w.-]/g, '') || email.replace(/@.*/, '').replace(/[^\w.-]/g, '');
  const hash = Math.floor(Date.now() / 1000 - 1510765697).toString(36); // unique hash in case of existing login
  const login = _login.padEnd(3, hash);
  return knex.raw(`INSERT INTO users (id, email, login, avatar)
    SELECT :id, TRIM(:email), CASE WHEN EXISTS(SELECT 1 FROM users WHERE LOWER(login)=LOWER(:login)) THEN (:login||:hash) ELSE :login END, :avatar
    ON CONFLICT (email)
    DO NOTHING
    RETURNING id, email, login, avatar, perm
  `, { id, email, login, hash, avatar }).then(({ rows }) => rows[0]);
};

/**
 * get user by id or email or login
 * @param {*} idOrEmailOrLogin
 * @param {*} session
 */
exports.get = (idOrEmailOrLogin, { user_id, perm }) => knex('users')
  .select(perm.read_perm || !idOrEmailOrLogin ? undefined : ['id', 'email', 'login', 'avatar', 'created'])
  .limit(1)
  .whereRaw(`(id=:q OR LOWER(email)=LOWER(:q) OR LOWER(login)=LOWER(:q))
  AND (
    :read_user OR
    id=:user_id OR
    EXISTS (SELECT 1 FROM users u JOIN managers m ON u.id=m.user_id AND m.manager_id=:user_id AND read_user)
  )`, { q: idOrEmailOrLogin || user_id, user_id, read_user: !!perm.read_user })
  .then(rows => {
    if (rows.length) {
      return rows[0];
    }
    throw new Error();
  });

/**
 * search
 * @param {*} query: { q: search in email or login, limit, skip }
 * @param {*} session
 */
exports.search = ({ q, limit = 100, skip = 0 }, { user_id, perm }) => {
  const query = knex('users')
    .select(['users.id', 'email', 'login', 'users.created', 'avatar', 'read_user'])
    .joinRaw('LEFT JOIN managers m ON user_id=users.id AND manager_id=:user_id', { user_id })
    .offset(skip)
    .limit(limit);
  if (typeof q === 'string' && q.trim()) {
    return query.whereRaw(`to_tsvector(email) || ' ' || to_tsvector(login) @@ plainto_tsquery(:q)
    AND (:read_user OR read_user)`, { q, read_user: !!perm.read_user });
  }
  return query.whereRaw(`:read_user OR read_user`, { read_user: !!perm.read_user });
};


/**
 * update user
 * @param {*} id
 * @param {*} data: { login, avatar, perm } // perm is filtered out if not session.perm.write_perm
 * @param {*} session
 */
exports.patch = (id, { login, avatar, perm: permObj }, { user_id, perm }) => knex('users')
  .update({
    login,
    avatar,
    perm: permObj && perm.write_perm && knex.raw(`perm || ?`, JSON.stringify(permObj))
  })
  .whereRaw(`id=:id AND (
    :write_user OR
    id=:user_id OR
    EXISTS (SELECT 1 FROM users u JOIN managers m ON u.id=m.user_id AND m.manager_id=:user_id AND write_user)
  )`, { id: id || user_id, user_id, write_user: !!perm.write_user })
  .catch(e => {
    console.error(e);
    throw new HttpError(403, e.message); // todo proper validation for the e.constraint hit
  });

/**
 * delete user (hard delete for now, we could soft delete with deleted=NOW())
 * @param {*} id
 * @param {*} session
 */
exports.delete = (id, { user_id, perm }) => knex('users')
  .delete()
  .whereRaw(`id=:id AND (
    :write_user OR
    id=:user_id OR
    EXISTS (SELECT 1 FROM users u JOIN managers m ON u.id=m.user_id AND m.manager_id=:user_id AND write_user)
  )`, { id: id || user_id, user_id, write_user: !!perm.write_user });
