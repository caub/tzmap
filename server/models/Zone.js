const shortid = require('shortid');
const tzlookup = require('tz-lookup'); // tzlookup(lat, lng) retuns timezone name
const { knex } = require('../util');

/*
postgis functions https://postgis.net/docs/using_postgis_dbmanagement.html 4.2.1
ST_asText geog -> text, ST_GeogFromText text -> geog

cols:
- id
- user_id
- name
- address
- coords // (geography), it would be more efficient to have a derived json column with [lng, lat] (*we put longitude first, like postgis, but tzlookup/leaflet expect lat first*)
- tz
*/

const rawCoords = knex.raw(`ST_asGeoJSON(coords)::json->'coordinates'`); // it should rather be computed column for perf

/**
 * get zone
 * @param {*} id
 * @param {*} session
 */
exports.get = (id, { user_id, perm }) => knex('zones')
  .select('*')
  .select({ coords: rawCoords })
  .whereRaw(`
  zones.id=:id AND (
    :read_zone OR
    zones.user_id=:user_id OR
    EXISTS (SELECT 1 FROM users u JOIN managers m ON u.id=m.user_id AND m.manager_id=:user_id AND read_zone)
  )`, { id, user_id, read_zone: !!perm.read_zone })
  .then(rows => rows[0]);

/**
 * query zones
 * @param {*} query: { q: text search, u: user_id (session.user_id by default), limit, skip }
 * @param {*} session
 */
exports.search = ({ q = '', u, limit = 100, skip = 0 }, { user_id, perm }) => {
  const query = knex('zones').select('zones.*').select({ coords: rawCoords })
    .join('users', 'users.id', 'zones.user_id')
    .limit(limit)
    .offset(skip);

  if (typeof q === 'string' && q.trim()) {
    return query.whereRaw(`
    (zones.user_id=:u OR users.login=:u OR users.email=:u) AND
    to_tsvector(coalesce(name,'')) || ' ' || to_tsvector(coalesce(address,'')) || ' ' || to_tsvector(coalesce(tz,'')) @@ plainto_tsquery(:q) AND (
      :read_zone OR
      zones.user_id=:user_id OR
      EXISTS (SELECT 1 FROM users u JOIN managers m ON u.id=m.user_id AND m.manager_id=:user_id AND read_zone)
    )`, { q, u: u || user_id, user_id, read_zone: !!perm.read_zone });
  }
  return query.whereRaw(`
  (zones.user_id=:u OR users.login=:u OR users.email=:u) AND (
    :read_zone OR
    zones.user_id=:user_id OR
    EXISTS (SELECT 1 FROM users u JOIN managers m ON u.id=m.user_id AND m.manager_id=:user_id AND read_zone)
  )`, { u: u || user_id, user_id, read_zone: !!perm.read_zone });
};


/**
 * post a new zone
 * @param {*} zoneObj: { address, coords, name }
 * @param {*} session
 */
exports.post = async ({ address, coords, name }, { user_id }) => {
  const id = shortid();
  const tz = tzlookup(coords[1], coords[0]);

  const [row] = await knex('zones')
    .insert({
      id,
      user_id,
      tz,
      name,
      address,
      coords: coords && knex.raw(`ST_geomFromGeoJSON(?)`, JSON.stringify({ type: 'Point', coordinates: coords }))
    })
    .returning([
      'id',
      'user_id',
      'name',
      'address',
      'tz',
      knex.raw(`ST_asGeoJSON(coords)::json->'coordinates' AS coords`)
    ]);

  return row;
};

/**
 * update zones
 * @param {*} id
 * @param {*} body: { coords, tz, address, name } // we could allow to change owner
 * @param {*} session: { user_id, type }
 */
exports.patch = (id, { coords, address, name }, { user_id, perm }) => knex('zones')
  .update({
    address,
    name,
    tz: coords && tzlookup(coords[1], coords[0]),
    coords: coords && knex.raw('ST_geomFromGeoJSON(?)', JSON.stringify({ type: 'Point', coordinates: coords }))
  })
  .whereRaw(`zones.id=:id AND (
    :write_zone OR
    zones.user_id=:user_id OR
    EXISTS (SELECT 1 FROM users u JOIN managers m ON u.id=m.user_id AND m.manager_id=:user_id AND write_zone)
  )`, { id, user_id, write_zone: !!perm.write_zone });

/**
 * delete a zone record
 * @param {*} id
 * @param {*} session
 */
exports.delete = (id, { user_id, perm }) => knex('zones')
  .delete()
  .whereRaw(`zones.id=:id AND (
  :write_zone OR
  zones.user_id=:user_id OR
  EXISTS (SELECT 1 FROM users u JOIN managers m ON u.id=m.user_id AND m.manager_id=:user_id AND write_zone)
)`, { id, user_id, write_zone: !!perm.write_zone });
