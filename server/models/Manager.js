const { HttpError } = require('../util');
const { knex } = require('../util');

/* cols:
- user_id
- manager_id

- read_user
- write_user
- read_zone
- write_zone
*/

/**
 * update (create or modify) manager perms
 * @param {*} body
 * @param {*} session
 */
exports.put = async (
  { user_id, manager_id, read_user, write_user, read_zone, write_zone },
  { perm }
) => {
  if (!perm.write_user) {
    throw new HttpError('Not allowed', 403);
  }
  if (!manager_id || !user_id) {
    throw new HttpError('Missing manager_id or user_id', 400);
  }
  return knex.raw(`INSERT INTO managers (user_id, manager_id, read_user, write_user, read_zone, write_zone)
  VALUES(?, ?, ?, ?, ?, ?)
  ON CONFLICT (user_id, manager_id)
  DO UPDATE SET
    read_user = coalesce(EXCLUDED.read_user, managers.read_user),
    write_user = coalesce(EXCLUDED.write_user, managers.write_user),
    read_zone = coalesce(EXCLUDED.read_zone, managers.read_zone),
    write_zone = coalesce(EXCLUDED.write_zone, managers.write_zone)
  RETURNING *
`, [user_id, manager_id, read_user, write_user, read_zone, write_zone]).then(({ rows }) => rows[0]);

  // return knex('managers')
  //   .limit(1)
  //   .where({
  //     user_id: userId,
  //     manager_id: managerId
  //   })
  //   .update({ read_user, write_user, read_zone, write_zone })
  //   .returning('*');
};
