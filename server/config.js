
exports.DB_URL = process.env.DB_URL || `pg://tt@localhost/tt${process.env.NODE_ENV === 'test' ? '_test' : ''}`;

exports.FULL_PERM = { read_user: true, write_user: true, read_perm: true, write_perm: true, read_zone: true, write_zone: true };

// https://www.w3.org/TR/html5/forms.html#e-mail-state-(type=email)
exports.EMAIL_RE = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

exports.LOGIN_RE = /^[\w.-]{3,}$/;

exports.G_API_KEY = 'WtYa70X9j-pw38G22PGDb5vz'; // optional
exports.G_CLIENT_ID = '160787631529-sqoapgtnctte7v41a9t3cg9u3p1sv72q.apps.googleusercontent.com';

exports.FB_APP_ID = '1141668375870723';

// for CORS
exports.ORIGINS_RE = /\/tzmap.herokuapp.com$|\/localhost:\d+$/;
