const router = require('express').Router();
const User = require('../models/User');

module.exports = fetch => {
  // used after getting token from google auth client-side SQK
  router.all('/google/:token', async (req, res, next) => {
    const token = req.params.token;
    // fetch('https://www.googleapis.com/oauth2/v3/tokeninfo?access_token='+req.params.token)
    const o = await fetch('https://www.googleapis.com/plus/v1/people/me?access_token=' + token).then(r => r.json());

    if (o.error) {
      return next(o.error);
    }

    User.signin({ email: o.emails[0].value, avatar: o.image && o.image.url, name: o.displayName })
      .then(user => {
        req.session.user_id = user.id;
        res.send(user);
      });
  });


  // used with FB auth SDK
  router.all('/facebook/:token', async (req, res, next) => {
    const token = req.params.token;
    const o = await fetch('https://graph.facebook.com/me?fields=email,locale,picture,name&access_token=' + token).then(r => r.json());

    if (o.error) {
      return next(o.error);
    }
    // avatar can also be fetched from `https://graph.facebook.com/v2.8/${r.id}/picture`
    User.signin({ email: o.email, avatar: o.picture && o.picture.data.url, name: o.displayName })
      .then(user => {
        req.session.user_id = user.id;
        res.send(user);
      });
  });

  return router;
};
