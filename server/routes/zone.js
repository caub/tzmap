const router = require('express').Router();
const Zone = require('../models/Zone');

router.use(require('body-parser').json());

router.all('*', (req, res, next) => {
  if (!req.session.user_id) {
    return res.status(403).send('Not signed in');
  }
  next();
});

router.search('/', (req, res) => {
  Zone.search(req.query, req.session)
    .then(zones => res.json(zones))
    .catch(() => res.status(403).end());
});

router.get('/:id', (req, res) => {
  Zone.get(req.params.id, req.session)
    .then(zone => res.json(zone))
    .catch(() => res.status(404).end());
});

router.post('/', (req, res) => {
  Zone.post(req.body, req.session)
    .then(zone => res.json(zone))
    .catch(() => res.status(404).end());
});

router.patch('/:id', (req, res) => {
  Zone.patch(req.params.id, req.body, req.session)
    .then(() => res.status(204).end())
    .catch(() => res.status(404).end());
});

router.delete('/:id', (req, res) => {
  Zone.delete(req.params.id, req.session)
    .then(() => res.status(204).end())
    .catch(() => res.status(404).end());
});

module.exports = router;
