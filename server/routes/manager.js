const router = require('express').Router();
const Manager = require('../models/Manager');

router.use(require('body-parser').json());

router.all('*', (req, res, next) => {
  if (!req.session.user_id) {
    return res.status(403).send('Not signed in');
  }
  next();
});

router.put('/', (req, res) => {
  Manager.put(req.body, req.session)
    .then(entry => res.json(entry))
    .catch(e => res.status(e.status).send(e.message));
});

module.exports = router;
