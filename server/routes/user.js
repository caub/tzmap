const router = require('express').Router();
const User = require('../models/User');

router.use(require('body-parser').json());

router.all('*', (req, res, next) => {
  if (!req.session.user_id) {
    return res.status(403).send('Not signed in');
  }
  next();
});

router.get('/:id?', (req, res) => {
  User.get(req.params.id, req.session)
    .then(user => res.json(user))
    .catch(_e => res.status(404).send('Not exists'));
});

router.search('/', (req, res) => {
  User.search(req.query, req.session)
    .then(users => res.json(users))
    .catch(_e => res.status(500).send('Unexpected error'));
});

router.patch('/:id?', (req, res) => {
  User.patch(req.params.id, req.body, req.session)
    .then(user => res.json(user))
    .catch(e => res.status(e.status).send(e));
});

router.delete('/:id', (req, res) => {
  User.delete(req.params.id, req.session)
    .then(() => res.status(204).end())
    .catch(e => res.status(403).send(e));
});

module.exports = router;
