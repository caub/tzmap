const fetch = require('node-fetch');
const { parse } = require('url');

const googleReq = url => {
  const o = JSON.parse(parse(url, true).query.access_token);
  return Promise.resolve({
    json: () => Promise.resolve({
      emails: [{ value: o.email }],
      image: { url: o.avatar },
      displayName: o.name
    })
  });
};

const facebookReq = url => {
  const o = JSON.parse(parse(url, true).query.access_token);
  return Promise.resolve({
    json: () => Promise.resolve({
      email: o.email,
      picture: { data: { url: o.avatar } },
      displayName: o.name
    })
  });
};

/**
 * fetch wrapper for letting OAuth2 calls to google and facebook pass in tests
 * @param {*} url
 * @param {*} opts
 */
const mockFetch = (url, opts) => url.startsWith('https://www.googleapis.com/') ?
  googleReq(url) : url.startsWith('https://graph.facebook.com/') ?
    facebookReq(url) :
    fetch(url, opts);

module.exports = mockFetch;
