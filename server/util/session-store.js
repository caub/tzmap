const { Store } = require('express-session');
const { knex } = require('.');

class PGStore extends Store {
  // constructor(opts) {
  //   super(opts);
  //   this.cols = opts.cols || ['user_id', 'perm']; // todo use it in get
  // }

  destroy(sid, cb) {
    return knex('sessions').where('id', sid).delete().then(() => cb());
  }

  get(sid, cb) {
    // console.log('get', sid);
    return knex.raw(`SELECT user_id, perm, cookie FROM sessions LEFT JOIN users ON users.id=sessions.user_id WHERE sessions.id=? AND expire > now()`, sid)
      .then(({ rows }) => cb(null, rows[0]));
  }

  set(sid, { user_id, cookie }, cb) {
    // console.log('set', sid, user_id);
    const expire = new Date(Date.now() + cookie.maxAge);
    return knex.raw(`INSERT INTO sessions (id, user_id, cookie, expire) VALUES (?, ?, ?, ?)
          ON CONFLICT (id)
          DO UPDATE SET user_id = EXCLUDED.user_id, cookie = EXCLUDED.cookie, expire = EXCLUDED.expire`, [sid, user_id, cookie, expire])
      .then(({ rows }) => cb(null, rows[0]));
  }
}

module.exports = opts => new PGStore(opts);
