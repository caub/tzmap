-- to initially create a new tt role and db

CREATE ROLE tt LOGIN; -- no password for now, easier, but it's better with
CREATE DATABASE tt WITH OWNER=tt;
GRANT ALL PRIVILEGES ON DATABASE tt TO tt;

CREATE DATABASE tt_test WITH OWNER=tt;
GRANT ALL PRIVILEGES ON DATABASE tt_test TO tt;

-- add postgis extension to store positions
\connect tt
CREATE EXTENSION postgis;

\connect tt_test
CREATE EXTENSION postgis;
