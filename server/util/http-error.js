module.exports = class HttpError extends Error {
  constructor(data = 'Server error', status) {
    const e = typeof data === 'string' ? { message: data } : data;
    super(e.message);
    this.status = status || e.status || 400;
    this.errors = e.errors;
  }

  toString() {
    return this.message;
  }

  toJSON() {
    return {
      message: this.message,
      errors: this.errors
    };
  }
};
