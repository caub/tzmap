const fetch = typeof window !== 'undefined' && window.fetch ? window.fetch : require('fetch-cookie')(require('node-fetch'));

const fetchHeaders = (url, { method = 'GET', body } = {}) => fetch(url, {
  method,
  body: body && JSON.stringify(body),
  credentials: 'include',
  headers: { 'Content-Type': 'application/json', Accept: '*/json' }
});

const toJson = r => (
  /^application\/json/.test(r.headers.get('Content-Type')) ? r.json() : r.text()
)
  .then(body => (
    r.ok ? body : Promise.reject(body)
  ));

/**
 * fetchJson helper
 * @param {*} url
 * @param {*} opts: { method, body } optional
 */
const fetchJson = (url, opts) => fetchHeaders(url, opts).then(toJson);

fetchJson.toJson = toJson;
fetchJson.fetchHeaders = fetchHeaders;

module.exports = fetchJson;
