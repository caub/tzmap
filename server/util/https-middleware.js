// redirect http -> https (except localhost)
module.exports = (req, res, next) => {
  const proto = req.headers['x-forwarded-proto'] || '';
  const isHttps = req.secure || proto.startsWith('https') || /^localhost:/.test(req.headers.host);
  if (!isHttps && (req.method === 'GET' || req.method === 'HEAD')) {
    return res.redirect(308, 'https://' + req.headers.host + req.originalUrl);
  }
  next();
};
