const crypto = require('crypto');
const { DB_URL } = require('../config');

exports.knex = require('knex')(DB_URL);

exports.HttpError = require('./http-error');

exports.gravatar = email => `https://gravatar.com/avatar/${crypto.createHash('md5').update(email).digest('hex')}?s=64&d=retro`;

