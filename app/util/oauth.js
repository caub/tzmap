/* global gapi, FB */

export function googleAuth(cb) {
  gapi.load('client:auth2', authGoogle);

  function authGoogle() {
    gapi.client.setApiKey('WtYa70X9j-pw38G22PGDb5vz');
    gapi.auth2.init({
      client_id: '160787631529-sqoapgtnctte7v41a9t3cg9u3p1sv72q.apps.googleusercontent.com',
      scope: 'email'
    }).then(() => {
      const auth2 = gapi.auth2.getAuthInstance();
      if (auth2.isSignedIn.get()) {
        return cb(auth2.currentUser.get().getAuthResponse().access_token);
      }
      gapi.auth2.getAuthInstance().signIn();
      auth2.isSignedIn.listen(_isSignedIn => {
        cb(auth2.currentUser.get().getAuthResponse().access_token);
      });
    });
  }
}

export function facebookAuth(cb) {
  FB.init({
    appId: '1141668375870723',
    xfbml: true,
    version: 'v2.8'
  });
  FB.getLoginStatus(r => {
    if (r.status === 'connected') {
      return cb(r.authResponse.accessToken);
    }
    FB.login(res => cb(res.authResponse.accessToken), { scope: 'email' });
  });
}
