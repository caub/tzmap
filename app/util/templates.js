import moment from 'moment-timezone';
import time from 'emoji-time';

window.moment = moment; // debug

// templates for leaflet

/**
 * template for adding a new timezone rendered in a leaflet marker popup
 * @param {*} address
 */
export const AddZoneTemplate = address => `<form class="zone-popup">
  <div class="address">
    ${address}
  </div>
  <label>
    <input type="text" name="name" placeholder="Add a name, ex: John's home" required>
  </label>
  <input type="submit" value="Submit">
  <div class="error"></div>
</form>`;

/**
 * zone popup
 * @param {*} zone
 */
export const ZoneTemplate = zone => {
  const t = moment.tz(zone.tz).format('HH:mm z');
  return `<div class="zone">
    <h3>
      <span>${time(t)}</span>
      <span class="name">${zone.name}</span>
      <span class="time" title="${t.split(' ', 1)[0] + ' ' + zone.tz}">${t}</span>
    </h3>
    <div class="address">${zone.address}</div>
  </div>`;
};
