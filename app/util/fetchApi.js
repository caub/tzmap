import fetchJson from '../../server/util/fetchJson';

const ORIGIN = process.env.ORIGIN || `http://localhost:${process.env.PORT || 3000}`;

const fetchApi = (endpoint, opts) => fetchJson(`${ORIGIN}/${endpoint}`, opts);

export default fetchApi;
