export const TOGGLE_SIDEBAR = 'TOGGLE_SIDEBAR';
export const CLOSE_SIDEBAR = 'CLOSE_SIDEBAR';
export const SIGNIN = 'SIGNIN';
export const TOGGLE_MODE = 'TOGGLE_MODE';
export const ADD_ZONE = 'ADD_ZONE';
export const SET_ZONES = 'SET_ZONES';
export const SET_ACTIVE_ZONE = 'SET_ACTIVE_ZONE';
export const DELETE_ZONE = 'DELETE_ZONE';
export const TOGGLE_NOTIFICATION = 'TOGGLE_NOTIFICATION';
export const ADD_NOTIFICATION = 'ADD_NOTIFICATION';
export const DELETE_NOTIFICATION = 'DELETE_NOTIFICATION';
export const EDIT_USER = 'EDIT_USER';
export const SET_USERS = 'SET_USERS';

const actions = {
  [TOGGLE_SIDEBAR]: state => ({ ...state, sidebarActive: !state.sidebarActive }),
  [CLOSE_SIDEBAR]: state => ({ ...state, sidebarActive: false }),
  [SIGNIN]: (state, session) => ({ ...state, session }),
  [TOGGLE_MODE]: (state, mode = state.mode === 'search' ? 'add' : 'search') => ({
    ...state,
    mode,
    sidebarActive: mode === 'add' ? false : state.sidebarActive
  }),
  [ADD_ZONE]: (state, zone) => ({
    ...state,
    zones: state.zones.concat(zone),
    mode: 'search'
  }),
  [SET_ZONES]: (state, zones) => ({ ...state, zones }),
  [SET_ACTIVE_ZONE]: (state, activeZone) => ({ ...state, activeZone }),
  [DELETE_ZONE]: (state, id) => ({ ...state, zones: state.zones.filter(z => z.id !== id) }),
  [TOGGLE_NOTIFICATION]: state => ({ ...state, notificationActive: !state.notificationActive }),
  [ADD_NOTIFICATION]: (state, notification) => ({
    ...state,
    notifications: (state.notifications || []).concat(notification)
  }),
  [DELETE_NOTIFICATION]: (state, id) => ({
    ...state,
    notifications: (state.notifications || []).filter(o => o.id !== id)
  }),
  [EDIT_USER]: (state, user) => ({
    ...state,
    users: (state.users || []).map(u => u.id === user.id ? { ...u, ...user } : u)
  }),
  [SET_USERS]: (state, users) => ({ ...state, users }),
};

export default function (state = {}, { type, value }) {
  const fn = actions[type];
  if (fn) {
    return fn(state, value);
  }
  if (!/^@/.test(type)) console.warn(type, 'not impl');
  return state;
}

// todo export below functions that do api calls, so they are testable (and pass dispatch to them)
// todo use react-thunk
let notificationId = 0;
export function pushNotification(data) {
  return dispatch => {
    const notification = { id: notificationId++, data };
    dispatch({ type: ADD_NOTIFICATION, value: notification });
    setTimeout(() => {
      dispatch({ type: DELETE_NOTIFICATION, value: notification.id });
    }, 5000);
  };
}
