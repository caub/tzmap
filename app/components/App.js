import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import styled from 'styled-components';
import Header from './Header';
import ZoneMap from './ZoneMap';
import UserProfile from './UserProfile';
import Signin from './Signin';
import ZoneList from './ZoneList';
import UserList from './UserList';
import Notification from './Notification';


const Aside = styled.aside`
  position: absolute;
  background: rgba(0,0,0,.5);
  z-index: 1002;
  top: 50px;
  bottom: 0;
  width: 300px;
  transform: translateX(${props => props.active ? '0' : '-300px'});
  transition: transform .3s linear;
  padding: 4px;
  text-align: right;
  overflow-y: auto;
  .btn.add {
    width: 100px;
    align-self: flex-end;
  }
  padding-top: 1em;
`;

const AppView = ({ session, sidebarActive }) => (
  <Router>
    <main>
      <Header />
      {session ? (
        <Switch>
          <Route exact path="/" component={ZoneMap} />
          <Route exact path="/~:user/zones" component={ZoneMap} />
          <Route path="/~:user?" component={UserProfile} />
          <Route render={() => <h2>Nothing here :(</h2>} />
        </Switch>
       ) : <Signin />}
      <Aside active={sidebarActive}>
        <Switch>
          <Route exact path="/" component={ZoneList} />
          <Route exact path="/~:user/zones" component={ZoneList} />
          <Route path="/~:user?" component={UserList} />
        </Switch>
      </Aside>
      <Notification />
    </main>
  </Router>
);

const mapStateToProps = (state, _p) => state;

const App = connect(mapStateToProps)(AppView);

export default App;
