import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { TOGGLE_NOTIFICATION } from '../util/reducers';

const Div = styled.div`
  position: absolute;
  z-index: 1002;
  bottom: 25px;
  right: 8px;
  width: 300px;
  list-style: none;
  margin: 0;
  border-radius: 4px;

  ul {
    overflow: hidden;
    max-height: ${props => props.active ? '400px' : '0'};
  }
  ul:empty + button {
    display: none;
  }
  li {
    padding: 15px 0 15px 20px;
    background: rgba(255,255,255,.8);
    transition: all .3s linear;
    overflow: hidden;
  }
  li.hide {
    opacity: 0;

  }
  span {
    display: inline-block;
    margin: 0 10px;
    vertical-align: middle;
  }
  .type {
    color: #c33;
  }

  .address {
    margin: 0;
    overflow: hidden;
    max-width: 220px;
    text-overflow: ellipsis;
  }

  button {
    position: absolute;
    top: -25px;
    right: -5px;
  }
  button::before {
    content: "${props => props.active ? '×' : '^'}";
    font-size: ${props => props.active ? '1.8em' : '2em'};
    transition: all .2s ease-in-out;
  }
`;

export const NotificationView = ({ notifications = [], notificationActive = true, onToggle }) => (
  <Div active={notificationActive}>
    <ul>
      {notifications.map(({ id, data }) => (
        <li key={id} className={data.expired ? 'hide' : ''}>
          {data}
        </li>
      ))}
    </ul>

    <button onClick={onToggle} title={(notificationActive ? 'Hide' : 'Show') + ' notifications'} />

  </Div>
);

const mapStateToProps = (state, _ownProps) => state;

const mapDispatchToProps = (dispatch, _ownProps) => ({
  onToggle: () => {
    dispatch({ type: TOGGLE_NOTIFICATION });
  }
});

const Notification = connect(mapStateToProps, mapDispatchToProps)(NotificationView);

export default Notification;
