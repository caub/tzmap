import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { SET_USERS } from '../util/reducers';
import fetchApi from '../util/fetchApi';

const Ul = styled.ul`
  color: #fafafa;
  .email {
    font-size: 90%;
  }
  li + li {
    border-top: 1px solid rgba(255,255,255,.4);
  }
  li {
    padding: 5px 15px;
    cursor: pointer;
  }
`;

const SearchForm = styled.form`
  align-self: flex-end;
  margin: 1em 6px;
  input {
    background-color: rgba(255,255,255,.7);
    font-size: 15.33px;
  }
`;

class UserListView extends React.PureComponent {
  componentDidUpdate(p) {
    if (p.sidebarActive !== this.props.sidebarActive && this.props.sidebarActive) {
      this.props.onFilter();
    }
  }

  render() {
    const { users = [], onFilter } = this.props;
    return [
      <Link to="/" key="b" className="btn back">Back</Link>,
      users.length ? (
        <SearchForm key="s" onSubmit={onFilter} onChange={onFilter}>
          <input type="search" name="search" placeholder="filter by name" />
        </SearchForm>
      ) : null,
      <Ul key="l">
        {users.map(user => (
          <li key={user.id}>
            <Link to={'/~' + user.login}>
              <div className="name">{user.login}</div>
              <div className="email">{user.email}</div>
            </Link>
          </li>
        ))}
      </Ul>
    ];
  }
}

const mapDispatchToProps = (dispatch, _ownProps) => ({
  onFilter: ({ currentTarget: form, type, preventDefault = () => {} } = {}) => {
    if (form && form.search.value && type === 'change') {
      return; // ignore non-reset changes for now
    }
    preventDefault();
    fetchApi('user?q=' + (form && form.search.value || ''), { method: 'SEARCH' })
      .then(users => dispatch({ type: SET_USERS, value: users }));
  }
});

const UserList = connect(state => state, mapDispatchToProps)(UserListView);

export default UserList;
