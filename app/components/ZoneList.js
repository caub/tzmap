import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import moment from 'moment-timezone';
import time from 'emoji-time';
import { TOGGLE_MODE, SET_ACTIVE_ZONE, DELETE_ZONE, SET_ZONES } from '../util/reducers';
import fetchApi from '../util/fetchApi';

const Li = styled.li`
  color: #fafafa;
  cursor: pointer;
  outline: none;
  > div {
    display: flex;
    align-items: center;
    padding: 4px 8px 4px 16px;
  }
  .name {
    flex: 1;
    font-size: 110%;
    padding: 6px 12px;
  }
  .time {
    font-size: 120%;
    padding-left: 20px;
  }
  :hover {
    background-color: rgba(255,255,255,.2);
  }
  :active, :focus {
    background-color: rgba(255,255,255,.5);
  }
  background-color: ${props => props.active ? 'rgba(255,255,255,.5)' : null};
`;

const SearchForm = styled.form`
  align-self: flex-end;
  margin: 1em 6px;
  input {
    background-color: rgba(255,255,255,.7);
    font-size: 15.33px;
  }
`;


const ZoneListView = ({ zones, activeZone, onAddZone, onSelect, onKey, onFilter }) => [
  <button
    key="add"
    className="btn add"
    title="Click on the map to create a timezone record"
    onClick={onAddZone}
  >
    New
  </button>,
  <SearchForm key="s" onSubmit={onFilter} onChange={onFilter}>
    <input type="search" name="search" placeholder="filter by name" />
  </SearchForm>,
  <ul key="l">
    {zones.map(zone => (
      <Li
        key={zone.id}
        tabIndex="0"
        active={zone.id === activeZone}
        innerRef={el => zone.id === activeZone ? el && el.focus() : el}
        onClick={() => onSelect(zone.id)}
        onKeyDown={e => onKey(e, zone.id)}
        title={zone.id === activeZone ? 'Press Delete key to delete' : null}
      >
        <div>
          <span className="name">{zone.name}</span>
          <span className="time">{time(moment.tz(zone.tz).format('HH:mm'))}</span>
        </div>
      </Li>
    ))}
  </ul>
];

const mapDispatchToProps = (dispatch, _ownProps) => ({
  onAddZone: () => {
    dispatch({ type: TOGGLE_MODE, value: 'add' });
  },
  onSelect: activeZone => {
    dispatch({ type: SET_ACTIVE_ZONE, value: activeZone });
  },
  onKey: (e, id) => {
    if (e.key === 'Delete') {
      fetchApi('zone/' + id, { method: 'DELETE' })
        .then(() => dispatch({ type: DELETE_ZONE, value: id }))
        .catch(console.error); // todo a Notification component bottom right
    }
    // todo handle ArrowUp/Down
  },
  onFilter: e => {
    if (e.currentTarget.search.value && e.type === 'change') {
      return; // ignore non-reset changes for now
    }
    e.preventDefault();
    fetchApi('zone?q=' + e.currentTarget.search.value, { method: 'SEARCH' })
      .then(zones => dispatch({ type: SET_ZONES, value: zones }));
  }
});

const ZoneList = connect(state => state, mapDispatchToProps)(ZoneListView);

export default ZoneList;
