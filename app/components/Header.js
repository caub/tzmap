import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import time from 'emoji-time';
import { Link } from 'react-router-dom';
import Dropdown, { DropdownContent } from './Dropdown';
import fetchApi from '../util/fetchApi';
import { TOGGLE_SIDEBAR, SIGNIN } from '../util/reducers';

const HeaderStyle = styled.header`
  min-height: 3.2em;
  display: flex;
  align-items: center;
  background: rgba(0,0,0,.5);
  position: relative;
  z-index: 1001;

  .menu {
    margin: 2px 8px;
    transition: all .2s ease-in-out;
    position: absolute;
  }
  .menu::before {
    content: "${props => props.active ? '×' : '☰'}";
    font-size: ${props => props.active ? '3em' : '2em'};
    color: #43c0e6;
  }
  .logo {
    color: #eee;
    margin: 0 auto;
    padding-right: 3em;
    @media (max-width: 800px) {
      margin: initial;
      margin-left: 2em;
    }
  }
  .logo::after {
    content: "TimeZone";
    margin-left: 6px;
    @media (max-width: 800px) {
      content: "Zone";
    }
  }
  .btn {
    margin: 5px;
    user-select: none;
  }
  .btn:first-child {
    @media (max-width: 700px) {
      display: none;
    }
  }
  .dropdown {
    position: absolute;
    right: 0;
  }
`;

const Button = styled.div`
  cursor: pointer;
  img {
    object-fit: contain;
    width: 42px;
    height: 42px;
    margin: 6px 8px 4px;
  }
`;

const Content = styled(DropdownContent)`
  padding: 10px;
  display: flex;
  align-items: center;
  img {
    object-fit: contain;
    width: 96px;
    height: 96px;
  }
  > div {
    display: flex;
    flex-direction: column;
    align-self: stretch;
  }
  .login {
    font-weight: 600;
  }
  .email, .login {
    padding: 4px;
  }
  .links {
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: space-evenly;
  }
  .btn {
    background-color: gray;
  }
`;

// todo update time in header with activeZone?

const HeaderView = ({ sidebarActive, session, onMenu, onLogout }) => (
  <HeaderStyle active={sidebarActive}>
    {session && <button className="menu" onClick={onMenu} />}
    <Link to="/" className="logo small" data-time={time()} />
    {session && (
      <Dropdown>{
        ({ toggle }) => [
          <Button key="button">
            <img src={session.avatar} alt={session.login} title={session.login} />
          </Button>,
          <Content key="content">
            <img src={session.avatar} alt={session.login} />
            <div>
              <div className="login">{session.login}</div>
              <div className="email">{session.email}</div>
              <div className="links">
                <Link className="btn" to={'/~' + session.login} onClick={() => toggle()}>
                  Edit
                </Link>
                <Link className="btn" to="/" onClick={onLogout}>
                  Log out
                </Link>
              </div>
            </div>
          </Content>
        ]
      }
      </Dropdown>
     )}
  </HeaderStyle>
);

const mapStateToProps = (state, _ownProps) => state;

const mapDispatchToProps = (dispatch, _ownProps) => ({
  onMenu: () => {
    dispatch({ type: TOGGLE_SIDEBAR });
  },
  onLogout: () => {
    fetchApi('logout')
      .then(() => dispatch({ type: SIGNIN }))
      .catch(console.error);
  }
});

const Header = connect(mapStateToProps, mapDispatchToProps)(HeaderView);

export default Header;
