import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { EDIT_USER, CLOSE_SIDEBAR, pushNotification } from '../util/reducers';
import fetchApi from '../util/fetchApi';

const Div = styled.div`
  margin: 3em auto;
  width: 300px;
  text-align: center;
  img {
    object-fit: contain;
    width: 96px;
    height: 96px;
  }
  .row {
    display: flex;
    align-items: center;
  }
  .login {
    display: flex;
    > * {
      margin-left: 2em;
      font-weight: 600;
      background-color: transparent;
      border: none;
      width: 130px;
      flex: 1;
      font-size: 1em;
      max-width: 100%;
      padding: 0;
      text-align: left;
    }
  }
  .email {
    margin: 1em;
  }
  .created {
    margin: 1em;
  }
  .tag {
    display: inline-block;
    background: #999;
    color: #fafafa;
    padding: 2px 4px;
    margin: 2px;
    font-size: 90%;
    user-select: none;
    ::after {
      content: attr(data-role);
    }
  }
  .tags {
    text-align: center;
  }
  .zones {
    display: inline-block;
    background-color: #43c0e6;
    margin: 2em;
  }
`;

class UserProfileView extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { login: '' };

    this.getUser = () => {
      const u = this.props.match.params.user;
      const sess = this.props.session;
      return (!u || u === sess.id || u === sess.login || u === sess.email) ?
        sess :
        this.state.user;
    };

    this.loadUser = idOrLoginOrEmail => {
      fetchApi('user/' + idOrLoginOrEmail)
        .then(user => {
          this.setState({ user });
        })
        .catch(err => this.props.pushNotification(err.message));
    };
  }

  componentDidMount() {
    if (this.props.match.params.user) {
      this.loadUser(this.props.match.params.user);
    }
  }
  componentDidUpdate(p) {
    if (p.match.params.user !== this.props.match.params.user && this.props.match.params.user) {
      this.loadUser(this.props.match.params.user);
    }
  }

  render() {
    const { onSubmit, onCloseSidebar } = this.props;
    const user = this.getUser();
    return user ? (
      <Div onClick={onCloseSidebar}>
        <div className="row">
          <img src={user.avatar} alt={user.login} />
          <form className="login" onSubmit={e => onSubmit(e, user.id)}>
            {this.state.login ?
              <input
                type="text"
                name="login"
                value={this.state.login}
                onChange={e => this.setState({ login: e.target.value })}
                onBlur={() => this.setState({ login: '' })}
                pattern="^[\w.-]{3,}$"
              /> :
              <span onDoubleClick={() => this.setState({ login: user.login })}>{user.login}</span>
            }
          </form>
        </div>
        <div className="email">{user.email}</div>
        <div className="created">
          <span>Created at: </span>
          <span>{new Date(user.created).toLocaleDateString()}</span>
        </div>
        <div className="tags">
          {Object.keys(user.perm || {}).map(role => <span key={role} className="tag" data-role={role} />)}
        </div>
        <Link to={'/~' + user.login + '/zones'} className="btn zones">See zones</Link>
      </Div>
    ) : <Div>Loading..</Div>;
  }
}

const mapStateToProps = (state, _ownProps) => state;

const mapDispatchToProps = (dispatch, _ownProps) => ({
  onSubmit: (e, id) => {
    if (e.preventDefault) {
      e.preventDefault();
    }
    if (e.currentTarget.login && e.currentTarget.login.value) {
      const login = e.currentTarget.login.value;
      fetchApi('user/' + id, { method: 'PATCH', body: { login } })
        .then(() => dispatch({ type: EDIT_USER, value: { login } }))
        .catch(err => this.props.pushNotification(err.message));
    }
  },
  onCloseSidebar: () => {
    dispatch({ type: CLOSE_SIDEBAR });
  },
  pushNotification: data => {
    pushNotification(data)(dispatch);
  }
});

const UserProfile = connect(mapStateToProps, mapDispatchToProps)(UserProfileView);

export default UserProfile;
