import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { googleAuth, facebookAuth } from '../util/oauth';
import fetchApi from '../util/fetchApi';
import { SIGNIN } from '../util/reducers';

const Div = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  flex: 1;
  color: #444;
  
  button:disabled {
    opacity: .3;
  }
  button {
    display: inline-flex;
    align-items: center;
  }
`;

const googleIcon = (
  <svg viewBox="0 0 48 48" width="30" height="30">
    <defs>
      <path id="a" d="M44.5 20H24v8.5h11.8C34.7 33.9 30.1 37 24 37c-7.2 0-13-5.8-13-13s5.8-13 13-13c3.1 0 5.9 1.1 8.1 2.9l6.4-6.4A21.94 21.94 0 0 0 2 24c0 12.2 9.8 22 22 22 11 0 21-8 21-22 0-1.3-.2-2.7-.5-4z" />
    </defs>
    <clipPath id="b"><use href="#a" overflow="visible" /></clipPath>
    <path clipPath="url(#b)" fill="#FBBC05" d="M0 37V11l17 13z" />
    <path clipPath="url(#b)" fill="#EA4335" d="M0 11l17 13 7-6.1L48 14V0H0z" />
    <path clipPath="url(#b)" fill="#34A853" d="M0 37l30-23 7.9 1L48 0v48H0z" />
    <path clipPath="url(#b)" fill="#4285F4" d="M48 48L17 24l-4-3 35-10z" />
  </svg>
);
const facebookIcon = (
  <svg viewBox="0 0 266.893 266.895" width="30" height="30">
    <path fill="#3C5A99" d="M248.08 262.3c7.86 0 14.22-6.36 14.22-14.22V18.81c0-7.85-6.36-14.22-14.22-14.22H18.81A14.22 14.22 0 0 0 4.6 18.8v229.27c0 7.86 6.36 14.23 14.22 14.23h229.27z" />
    <path fill="#FFF" d="M182.4 262.3v-99.8h33.5l5.02-38.9h-38.51V98.79c0-11.26 3.13-18.94 19.27-18.94h20.6v-34.8c-3.56-.47-15.79-1.53-30.01-1.53-29.7 0-50.03 18.13-50.03 51.41v28.69h-33.58v38.9h33.58v99.8h40.17z" />
  </svg>
);


export const SigninView = ({ onGoogleAuth, onFacebookAuth }) => (
  <Div>
    <h3>Please sign in with Google or Facebook</h3>
    <div>
      <button className="btn google" onClick={onGoogleAuth}>{googleIcon}</button>
      <button className="btn facebook" onClick={onFacebookAuth}>{facebookIcon}</button>
    </div>
  </Div>
);


const mapStateToProps = (state, _ownProps) => state;

const mapDispatchToProps = (dispatch, _ownProps) => ({
  onGoogleAuth() {
    googleAuth(token => {
      fetchApi('signin/google/' + token)
        .then(user => {
          dispatch({ type: SIGNIN, value: user });
        });
    });
  },
  onFacebookAuth() {
    facebookAuth(token => {
      fetchApi('signin/facebook/' + token)
        .then(user => {
          dispatch({ type: SIGNIN, value: user });
        });
    });
  }
});

const Signin = connect(mapStateToProps, mapDispatchToProps)(SigninView);

export default Signin;
