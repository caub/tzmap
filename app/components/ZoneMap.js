import React from 'react';
import { connect } from 'react-redux';
import L from 'leaflet';
import esri from 'esri-leaflet';
import geocoding from 'esri-leaflet-geocoder';
import terminator from 'leaflet.terminator';
import tzlookup from 'tz-lookup';
import moment from 'moment-timezone';
import time from 'emoji-time';
import { CLOSE_SIDEBAR, ADD_ZONE, SET_ACTIVE_ZONE, SET_ZONES, pushNotification } from '../util/reducers';
import { AddZoneTemplate, ZoneTemplate } from '../util/templates';
import fetchApi from '../util/fetchApi';

// todo setTimeouts outside this comp (that can be unmounted)

class ZoneMapView extends React.PureComponent {
  constructor(props) {
    super(props);

    this.updateTime = () => {
      window.requestAnimationFrame(() => {
        this.term.setTime(new Date());
        this.props.zones.forEach(zone => {
          const marker = this.markers.get(zone.id);
          if (marker && !marker.isPopupOpen()) {
            marker.getPopup().setContent(ZoneTemplate(zone));
          }
        });
        if (this.props.activeZone) {
          this.updateDocumentTitle();
        }
        setTimeout(this.updateTime, 60000);
      });
    };

    this.updateDocumentTitle = () => {
      const zone = (this.props.zones || []).find(z => z.id === this.props.activeZone); // we should use a Map
      if (zone) {
        const t = moment.tz(zone.tz).format('HH:mm');
        document.title = time(t) + ' ' + zone.name;
      }
    };

    this.loadZones = () => {
      const u = this.props.match.params.user;
      const sess = this.props.session;
      const idOrLoginOrEmail = (u && u !== sess.id && u !== sess.login && u !== sess.email) ? u : '';
      fetchApi('zone?u=' + idOrLoginOrEmail, { method: 'SEARCH' })
        .then(zones => {
          this.props.onSetZones(zones);
        })
        .catch(err => this.props.pushNotification(err.message));
    };

    this.setZones = zones => {
      this.searchResults.clearLayers();
      this.markers = new Map(); // id => marker
      window.markers = this.markers; // debug

      zones.forEach(zone => {
        const marker = L.marker([zone.coords[1], zone.coords[0]], { draggable: true }).bindPopup(ZoneTemplate(zone));
        this.searchResults.addLayer(marker);
        this.markers.set(zone.id, marker);
        marker.on('click', () => this.props.onSelect(zone.id));
        marker.on('dragend', e => {
          const latlng = e.target.getLatLng();
          this.geocodeService.reverse().latlng(latlng).run((error, result) => {
            const address = error ? zone.address : result.address.Match_addr;
            const tz = tzlookup(latlng.lat, latlng.lng);

            // todo: I should return new data from api, and avoid mutating
            fetchApi('zone/' + zone.id, { method: 'PATCH', body: { coords: [latlng.lng, latlng.lat] } })
              .then(() => {
                zone.tz = tz;
                zone.address = address;
                marker.getPopup().setContent(ZoneTemplate(zone));
              })
              .catch(err => this.props.pushNotification(err.message));
          });
        });
      });
    };
  }

  componentDidMount() {
    this.loadZones(); // should be done earlier, todo

    this.props.history.listen((_location, _action) => {
      this.props.onMapClick();
    });

    // init leaflet map
    this.map = L.map('map', {
      minZoom: 2,
      maxBounds: [[-85, -260], [85, 320]]
    }).setView([30, 0], 2);
    window.map = this.map; // debug

    esri.basemapLayer('Streets').addTo(this.map);

    const _searchControl = geocoding.geosearch().addTo(this.map);

    this.searchResults = L.layerGroup().addTo(this.map);

    this.setZones(this.props.zones);

    this.geocodeService = geocoding.geocodeService();

    this.geocodeResults = L.layerGroup().addTo(this.map);

    this.term = terminator().addTo(this.map);

    setTimeout(this.updateTime, 60000);

    // this.map.invalidateSize();

    this.map.on('click', e => {
      this.props.onMapClick();

      if (this.props.mode === 'add') {
        this.geocodeService.reverse().latlng(e.latlng).run((error, result) => {
          this.geocodeResults.clearLayers();
          if (error) {
            return console.error(error);
          }

          const marker = L.marker(result.latlng).bindPopup(AddZoneTemplate(result.address.Match_addr));
          this.geocodeResults.addLayer(marker);
          marker.openPopup();

          const form = marker.getPopup().getElement().querySelector('form');

          form.addEventListener('submit', evt => {
            evt.preventDefault();
            const zoneData = {
              address: result.address.Match_addr,
              coords: [result.latlng.lng, result.latlng.lat],
              name: form.name.value,
            };
            fetchApi('zone', { method: 'POST', body: zoneData })
              .then(r => {
                this.map.closePopup();
                this.geocodeResults.clearLayers();
                this.props.onAdd(r);
              })
              .catch(err => {
                console.log(err);
                form.querySelector('.error').innerText = err.message;
              });
          });
        });
      }
    });
  }

  componentDidUpdate(p) {
    // redraw zones (todo check p.zones vs this.props.zones)
    if (p.zones !== this.props.zones) {
      this.setZones(this.props.zones);
    }
    if (p.activeZone !== this.props.activeZone) {
      const marker = this.markers.get(this.props.activeZone);
      if (marker) {
        marker.openPopup();
        this.map.flyTo(marker.getLatLng());
      }
      this.updateDocumentTitle();
    }
    if (p.match.params.user !== this.props.match.params.user) {
      this.loadZones();
    }
  }

  render() {
    return (
      <div id="map" style={this.props.mode === 'add' ? { cursor: 'copy' } : null} />
    );
  }
}

const mapStateToProps = (state, _ownProps) => state;

const mapDispatchToProps = (dispatch, _ownProps) => ({
  onMapClick: () => {
    dispatch({ type: CLOSE_SIDEBAR });
  },
  onAdd: zone => {
    dispatch({ type: ADD_ZONE, value: zone });
  },
  onSelect: activeZone => {
    dispatch({ type: SET_ACTIVE_ZONE, value: activeZone });
  },
  onSetZones: zones => {
    dispatch({ type: SET_ZONES, value: zones });
  },
  pushNotification: data => {
    pushNotification(data)(dispatch);
  }
});

const ZoneMap = connect(mapStateToProps, mapDispatchToProps)(ZoneMapView);

export default ZoneMap;
