import React from 'react';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';
import eq from 'deep-eq';

import { SigninView } from '../Signin';

it('should render google and facebook buttons', () => {
  const wrapper = shallow(<SigninView />);
  eq(wrapper.find('button').length, 2);
});

it('should click on google and facebook buttons', () => {
  const onGoogleAuth = sinon.spy();
  const wrapper = mount(<SigninView onGoogleAuth={onGoogleAuth} />);
  wrapper.find('button.google').simulate('click');
  eq(onGoogleAuth.calledOnce, true);
});
